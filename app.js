/*

    app.js

*/

//define uma instância para o express
var express = require('express');
var app = express();

//define o EJS como nossa view engine
app.set('view engine', 'ejs');

//Usar res.render para carregar arquivos de view ejs

//index page
app.get('/', function(req, res){

    var bebidas = 
    [{ name: 'Bloody Mary', drunkness: 3 },
     { name: 'Martini', drunkness: 5 },
     { name: 'Scotch', drunkness: 10 }];

    res.render('pages/index', {
        bebidas: bebidas
    });

});

//about page
app.get('/about', function(req, res){
	res.render('pages/about');
});

app.listen(8080);
console.log('8080 é a porta mágica');